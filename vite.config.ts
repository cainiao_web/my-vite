import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import IconsResolver from 'unplugin-icons/resolver'
import Icons from 'unplugin-icons/vite'

// https://vitejs.dev/config/
const pathSrc = resolve(__dirname, 'src')

export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({prefix: 'Icon'}),
      ],
      // 自动生成components.d.ts文件
      dts: resolve(pathSrc, 'components.d.ts'),
    }),
    Components({
      resolvers: [
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({
          // ep 是 Element Plus 的缩写
          enabledCollections: ['ep'],
        })
      ],
      // 自动生成components.d.ts文件
      dts: resolve(pathSrc, 'components.d.ts'),
    }),
    Icons({autoInstall: true}),
  ],
  define: {
    'process.env': {}
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, './src')
    }
  },

  base: './',
  server: {
    port: 9000,
    open: true,
    cors: true,
    // 设置代理，根据我们项目实际情况配置
    // proxy: {
    //   '/api': {
    //     target: 'http://192.168.101.17:5200', //接口域名
    //     changeOrigin: true, //是否跨域
    //     // ws: true, //是否代理 websockets
    //     secure: false, //是否https接口
    //     rewrite: (path) => path.replace('/api/', '/')
    //   }
    // }
  }
})


