import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

// import LoginPage from '../views/login/loginPage.vue'
// import Layout from '@/layout/index.vue'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        redirect: '/home',
    },
    {
        path: '/home',
        name: 'home',
        component:  () => import(/* webpackChunkName: "about" */ '@/views/home/home.vue'),
        meta: {
            title: '旅游'
        }
    },
    {
        path: '/train',
        name: 'train',
        component:  () => import(/* webpackChunkName: "about" */ '@/views/train/index.vue'),
        meta: {
            title: '火车'
        }
    },
    {
        path: '/aircraft',
        name: 'aircraft',
        component:  () => import(/* webpackChunkName: "about" */ '@/views/aircraft/index.vue'),
        meta: {
            title: '飞机'
        }
    },
//   {
//     path: '/',
//     redirect: '/login',
//   },
//   {
//     path: '/login',
//     name: 'login',
//     component: LoginPage
//   },
//   {
//     path: '/home',
//     component: Layout,
//     redirect: '/homePage',
//     children: [{
//       path: '/homePage',
//       name: 'homePage',
//       component:  () => import(/* webpackChunkName: "about" */ '@/views/home/homePage.vue')
//     },{
//       path: '/personalCenter',
//       name: 'personalCenter',
//       component:  () => import(/* webpackChunkName: "about" */ '@/views/personal/personalCenter.vue')
//     },{
//       path: '/cooperateCenter',
//       name: 'cooperateCenter',
//       component:  () => import(/* webpackChunkName: "about" */ '@/views/cooperate/cooperateCenter.vue')
//     },{
//       path: '/productCenter',
//       name: 'productCenter',
//       component:  () => import(/* webpackChunkName: "about" */ '@/views/product/productCenter.vue')
//     },{
//       path: '/dataCenter',
//       name: 'dataCenter',
//       component:  () => import(/* webpackChunkName: "about" */ '@/views/data/dataCenter.vue')
//     }]
//   }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
