const cityList = {

    "CHotCity": [
        {
            "id": 1,
            "displayName": "北京",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 2,
            "displayName": "上海",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 3,
            "displayName": "天津",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 4,
            "displayName": "重庆",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 6,
            "displayName": "大连",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 7,
            "displayName": "青岛",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 10,
            "displayName": "西安",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 12,
            "displayName": "南京",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 14,
            "displayName": "苏州",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 17,
            "displayName": "杭州",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 25,
            "displayName": "厦门",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 28,
            "displayName": "成都",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 30,
            "displayName": "深圳",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 32,
            "displayName": "广州",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 43,
            "displayName": "三亚",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 617,
            "displayName": "台北",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 58,
            "displayName": "香港",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 144,
            "displayName": "济南",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 375,
            "displayName": "宁波",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 451,
            "displayName": "沈阳",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 477,
            "displayName": "武汉",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        },
        {
            "id": 559,
            "displayName": "郑州",
            "sort": 0,
            "timeOffset": 28800,
            "oversea": 0,
            "type": 0,
            "countryId": 1,
            "provinceId": 0
        }
    ],
    "UHotCity": [
        {
            "id": 274,
            "displayName": "首尔",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 42,
            "provinceId": 0
        },
        {
            "id": 359,
            "displayName": "曼谷",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 725,
            "displayName": "普吉岛",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 228,
            "displayName": "东京",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 78,
            "provinceId": 0
        },
        {
            "id": 73,
            "displayName": "新加坡",
            "sort": 0,
            "timeOffset": 0,
            "oversea": 1,
            "type": 0,
            "countryId": 3,
            "provinceId": 0
        },
        {
            "id": 219,
            "displayName": "大阪",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 78,
            "provinceId": 0
        },
        {
            "id": 737,
            "displayName": "济州市",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 42,
            "provinceId": 0
        },
        {
            "id": 723,
            "displayName": "巴厘岛",
            "sort": 0,
            "timeOffset": 0,
            "oversea": 1,
            "type": 0,
            "countryId": 108,
            "provinceId": 0
        },
        {
            "id": 623,
            "displayName": "清迈",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 1393,
            "displayName": "哥打京那巴鲁",
            "sort": 0,
            "timeOffset": 0,
            "oversea": 1,
            "type": 0,
            "countryId": 2,
            "provinceId": 0
        },
        {
            "id": 734,
            "displayName": "京都",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 78,
            "provinceId": 0
        },
        {
            "id": 315,
            "displayName": "吉隆坡",
            "sort": 0,
            "timeOffset": 0,
            "oversea": 1,
            "type": 0,
            "countryId": 2,
            "provinceId": 0
        },
        {
            "id": 622,
            "displayName": "芭堤雅",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 92573,
            "displayName": "那霸",
            "sort": 0,
            "timeOffset": 3600,
            "oversea": 1,
            "type": 0,
            "countryId": 78,
            "provinceId": 0
        },
        {
            "id": 347,
            "displayName": "洛杉矶",
            "sort": 0,
            "timeOffset": -57600,
            "oversea": 1,
            "type": 0,
            "countryId": 66,
            "provinceId": 0
        },
        {
            "id": 1229,
            "displayName": "苏梅岛",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 192,
            "displayName": "巴黎",
            "sort": 0,
            "timeOffset": -25200,
            "oversea": 1,
            "type": 0,
            "countryId": 31,
            "provinceId": 0
        },
        {
            "id": 1405,
            "displayName": "甲米",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 4,
            "provinceId": 0
        },
        {
            "id": 26282,
            "displayName": "拉斯维加斯",
            "sort": 0,
            "timeOffset": -57600,
            "oversea": 1,
            "type": 0,
            "countryId": 66,
            "provinceId": 0
        },
        {
            "id": 338,
            "displayName": "伦敦",
            "sort": 0,
            "timeOffset": -28800,
            "oversea": 1,
            "type": 0,
            "countryId": 109,
            "provinceId": 0
        },
        {
            "id": 633,
            "displayName": "纽约",
            "sort": 0,
            "timeOffset": -46800,
            "oversea": 1,
            "type": 0,
            "countryId": 66,
            "provinceId": 0
        },
        {
            "id": 1777,
            "displayName": "芽庄",
            "sort": 0,
            "timeOffset": -3600,
            "oversea": 1,
            "type": 0,
            "countryId": 111,
            "provinceId": 0
        },
        {
            "id": 501,
            "displayName": "悉尼",
            "sort": 0,
            "timeOffset": 10800,
            "oversea": 1,
            "type": 0,
            "countryId": 15,
            "provinceId": 0
        }
    ]
}

export default cityList