/// <reference types="vite/client" />
declare module 'element-plus/dist/locale/zh-cn.mjs'
declare module 'element-plus/dist/locale/en.mjs'
declare module '@/axios/global.js'

declare module '*.vue' {
    import type { DefineComponent } from 'vue';

    const vueComponent: DefineComponent<{}, {}, any>;

    export default vueComponent;

}

// declare module '*./router' {
//     import type { DefineComponent } from 'vue-router'
//     const component: DefineComponent<{}, {}, any>
//     export default component
//   }