import axios from 'axios'
// import store from '@/store'
import router from '@/router'
import { ElMessage } from 'element-plus'
import { BASE_URL } from '@/axios/global.js'

// create an axios instance
const service = axios.create({
    // process.env.VUE_APP_BASE_API
    baseURL: BASE_URL, // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 60000 // request timeout
})

// request interceptor
service.interceptors.request.use(
    config => {
        // do something before request is sent
        // config.cancelToken = new axios.CancelToken(function (cancel) {
        //     store.commit('pushToken', { cancelToken: cancel })
        // })
        config.headers['content-type'] = 'application/json'
        const token = sessionStorage.getItem('token');
        // config.headers['x-token'] = `${token}`;
        config.headers['Authorization'] = `${token}`;
        // config.headers['uServiceId'] = `${sessionStorage.getItem('wsTime')}`;

        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
    */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data
        // if the custom code is not 20000, it is judged as an error.
        if (res.code === 200 || res.code === 201) {
            //去掉res return res.data || res 
            if (res.data == 0) {
                return res.data
            } else {
                return res.data || res
            }
        } else if (res.code === 30112) {
            return res
        } else {
            ElMessage({
                message: res.data || res.msg || 'Error',
                type: 'error',
                duration: 5 * 1000
            })

            // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
            if (res.code === '-101' || res.code === '-103' || res.code === 50014) {
                sessionStorage.clear()
                console.log(2222222)
                router.replace({path: '/login'})
                // location.reload()
            }
            return Promise.reject(res.msg || 'Error')
        }
    },
    error => {
        console.log('err' + error) // for debug
        if (error.message != 'cancel') {
            ElMessage({
                message: error.message,
                type: 'error',
                duration: 5 * 1000
            })
        }
        return Promise.reject(error)
    }
)
export default service
