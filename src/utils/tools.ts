import dayjs from 'dayjs'
import duration from "dayjs/plugin/duration";
dayjs.extend(duration); // 使用插件
dayjs.locale("zh-cn"); // 使用本地化语言

// 时间格式化
export const timeFormatter = (val: any, format: any = 'YYYY-MM-DD') => {
    return dayjs(val).format(format)
}

// 获取时间差
export const timeDiff = (startTime: any, endTime: any) => {
    const diffDays = dayjs(endTime).diff(dayjs(startTime)) / (24 * 60 * 60 * 1000)
    return diffDays
}

// 获取星期
export const getWeek = (val:any) => {
    const week = ['日', '一', '二', '三', '四', '五', '六']
    return `星期${week[val]}`
}