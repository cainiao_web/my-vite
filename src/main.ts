import { createApp } from 'vue'
import './styles/index.css'
import App from './App.vue'
// import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import './styles/element-ui.scss'

import 'qweather-icons/font/qweather-icons.css'

import router from './router'
import pinia from '@/stores'

const app = createApp(App)

// app.use(ElementPlus, { zIndex: 3000 , locale: zhCn })
app.use(pinia).use(router).mount('#app')