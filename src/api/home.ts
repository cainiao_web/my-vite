import request from '@/utils/request'

import { url_name } from '@/axios/global.js'

// 登录
export function getDict(data:any) {
    return request({
        url: `${url_name}/getDict`,
        method: 'post',
        data
    })
}

// 登录
export function getCity() {
    return request({
        url: `${url_name}/getCity`,
        method: 'post'
    })
}

// 获取推荐酒店
export function getRecommendHotal(data:any) {
    return request({
        url: `${url_name}/getRecommendHotal`,
        method: 'post',
        data
    })
}


// 获取热门旅游
export function getHotTravels(data:any) {
    return request({
        url: `${url_name}/getHotTravels`,
        method: 'post',
        data
    })
}

// 获取特价机票
export function getHotFlights(data:any) {
    return request({
        url: `${url_name}/getHotFlights`,
        method: 'post',
        data
    })
}

// 获取热门推荐酒店
export function getRecommendCityHotal() {
    return request({
        url: `${url_name}/getRecommendCityHotal`,
        method: 'post'
    })
}

