1、组件、icon按需引入需第三方插件

```
npm install -D unplugin-vue-components unplugin-auto-import unplugin-icons
```

2、vite.config.js配置  [myVite\vite.config.ts](vite.config.ts)

3、配置完后，组件可以直接使用，icon需要另外写法

4、icon使用，需要在图标名称前面加IEp

```
<el-icon><IEpSearch /></el-icon>
```

5、按钮等元素内图标无法这样使用，如需使用

    1）使用slot

    2）先引入图标，然后按官网方式使用
